// initialize the controls in the input data template and validate residence type
$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();
$('.ui.form')
.form({
	residenceType : {
		identifier : 'restype',
		rules: [
		  {
			  	type : 'empty',
			  	prompt: 'Please select a residence type'
		  }
	  ]
	}
});

