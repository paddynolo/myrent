/*
var elements = document.getElementsById('location');
var vacancy = Array();
for(var i = 0; i < elements.length; i++)
{
  vacancy.push(elements[i].value);
}
*/
var vacancy = [    ["${residence.eircode}" , "${residence.location}"],          
                   ['eircode', 53.447298,-6.368344,4],
                   ['eircode', 53.547298,-6.468344,3],
                   ['eircode', 53.647298,-6.568344,2],
                   ['eircode', 53.747298,-6.668344,1]
               ];

var circle;
function requestReport() {
    var center = circle.getCenter();
    var latcenter = center.lat().toString();
    var lngcenter = center.lng().toString();
    var radius = circle.getRadius().toString();
    //alert('radius');
    $("#radius").val(radius);
    $("#latcenter").val(latcenter);
    $("#lngcenter").val(lngcenter);    
}

function initialize()
{
    var center =new google.maps.LatLng(53.347298,-6.268344);
    var initRadius = 10000;
    var mapProp = {
            center:center,
            zoom:7,
            mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var mapDiv = document.getElementById("map_canvas");
    var map = new google.maps.Map(mapDiv,mapProp);
    mapDiv.style.width = '500px';
    mapDiv.style.height = '500px';

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < vacancy.length; i++) 
    {  
      marker = new google.maps.Marker
      ({
        position: new google.maps.LatLng(vacancy[i][1], vacancy[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) 
      {
        return function() 
        {
          infowindow.setContent(vacancy[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
     }
    
      circle = new google.maps.Circle({
          center:center,
          radius:initRadius,
          strokeColor:"#0000FF",
          strokeOpacity:0.4,
          strokeWeight:1,
          fillColor:"#0000FF",
          fillOpacity:0.4,
          draggable: true
          });
      
      circle.setEditable(true);//allows varying radius be dragging anchor point
      circle.setMap(map);
  }
  google.maps.event.addDomListener(window, 'load', initialize);
