package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
public class Administrators extends Controller {


public static void login(){
	
	   session.clear();
	   render();
    }

public static void home(){
    
	render();
	
}

public static void logout(){
	
	   session.remove("logged_in_administrator");
	   Welcome.index();
}

public static void authenticate(String email, String password){
	
	Logger.info("Attempt to Authenticate with"+email+":"+password);

Administrator administrator = Administrator.findByEmail(email);
 if ((administrator != null)&&(administrator.checkPassword(password)==true)){
	 Logger.info("Successful Authentication of "+administrator.admin);
 	 session.put("logged_in_landlord",administrator.id);
	  Administrators.home();
 }

 else{
	 Logger.info("Authentication Failed");
	 login();
 }
}

public static Administrator getCurrentAdministrator(){
	
	String userId = session.get("logged_in_administrator");
	if (userId == null)
	{
	Logger.info("Failed to get Current Admin");
	return null;
	}
	Administrator loggered_in_administrator = Administrator.findById(Long.parseLong(userId));
	return loggered_in_administrator;
}


public static void signupLandlord(){
    
	Landlords.newlandlord();
	render();
	
}

public static void signupTenant(){
    
	Tenants.newtenant();
	render();
	
}


public static void deletepage(){
	
    
    List<Landlord> landlords = new ArrayList<Landlord>();
    List<Tenant> tenants = new ArrayList<Tenant>();
    List<Residence> residences = new ArrayList<Residence>();
   
    List<Landlord> landlordsAll = Landlord.findAll();
    List<Tenant> tenantsAll = Tenant.findAll();
    List<Residence> residencesAll = Residence.findAll();
    
    for (Landlord landlord : landlordsAll)
    {
        landlords.add(landlord);
      
    }
    for (Tenant tenant : tenantsAll)
    {
        tenants.add(tenant);
      
    }
    for (Residence residence : residencesAll)
    {
        residences.add(residence);
      
    }
    render("Administrators/deletepage.html", landlords,tenants,residences);
    
}	




public static void deletingpagerr(Residence residence,Tenant tenant, Landlord landlord){
 
	
    if(Residence.findById(residence.id)!=null){
    
      System.out.print(residence.id+"  will be deleted");
      session.get("logged_in_resid");
   
      render(landlord,residence,tenant);
      deleteRes(residence);
    }
   }

public static void deletingpagell(Residence residence,Tenant tenant, Landlord landlord){

    
    if(Landlord.findById(landlord.id)!=null){
    
      System.out.print(landlord.id+"  will be deleted");
      session.get("logged_in_landlord");
   
      render(landlord);
      deleteLand(landlord);
     }
    }
    
public static void deletingpagett(Residence residence,Tenant tenant, Landlord landlord){

    if(Tenant.findById(tenant.id)!=null){
    
      System.out.print(tenant.id+"  will be deleted");
      session.get("logged_in_tenant");
   
      render(tenant);
      deleteTen(tenant);
    }
    
    else{
    	home();
    }
    
    

} 


public static void deleteRes(Residence residence){		 		 		   

 if(residence != null){
      System.out.println(residence.id + " is the one");
      	
      residence.delete();
      
      home();}

    else{
    	System.out.print("no joy");
    	home();}
    	        
}

public static void deleteLand(Landlord landlord){		 		 		   
    
	    if(landlord!=null){
	    
	      System.out.println(landlord.id + " is the one");
	      	
	      landlord.delete();
	      
	      home();}

	    else{
	    	System.out.print("no joy");
	    	home();}
	    	        
	}

public static void deleteTen(Tenant tenant){		 		 		   

	
	    
	    if(tenant!=null){
	    
	      System.out.println(tenant.id + " is the one");	
	      tenant.delete();
	      home();}

	    else{
	    	System.out.print("no joy");
	    	home();}
	    	        
	}




}