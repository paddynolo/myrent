package controllers;

import play.*;
import play.mvc.*;
import utils.Geodistance;
import utils.LatLng;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
   

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import models.*;
public class Residences extends Controller {

	
	
	
	public static Residence getCurrentResidence(){
		
		String resId = session.get("verified_res_location");
		if (resId == null)
		{
			Logger.info("Failed to get Current Residence");
		return null;
		}
		Residence residence_in_tables = Residence.findById(Long.parseLong(resId));
		return residence_in_tables;
	}
	
	
	
    public static Residence getCurrentResID(){
		
		String resId = session.get("verified_res_id");
		if (resId == null)
		{
			Logger.info("Failed to get Current Residence");
		return null;
		}
		Residence residence_in_tables = Residence.findById(Long.parseLong(resId));
		return residence_in_tables;
	}
    
public static Residence getCurrentResAdminID(Residence residence){
		
		String resId = session.get("verified_res_id");
		if (resId == null)
		{
			Logger.info("Failed to get Current Residence");
		return null;
		}
		Residence residence_in_tables = Residence.findById(Long.parseLong(resId));
		return residence_in_tables;
	}
	
	
   public static void editproperty(){
		
	    String LLref =  session.get("logged_in_landlord");
	    Logger.info("Session user "+LLref);
	    
	    Landlord landlord = Landlords.getCurrentLandlord();
	   
	    List<Residence> residences = new ArrayList<Residence>();
	    // Fetch all residences and filter out those with landlord id
	    List<Residence> residencesAll = Residence.findAll();
	    
	    for (Residence residence : residencesAll)
	    {	      
	      if (residence.landlord == landlord )
	      {
	        residences.add(residence);
	      }
	    }
	    
	    render("Residences/editproperty.html", landlord,residences);
	       
   }	
   
   
	 
   public static void editingpage(Residence residence, Landlord landlord){
		
		
		
		  authenticate(residence.id);
		  Logger.info("verified_res_id", +residence.id);
		 				 
				 if(authenticate(residence.id) == true){
			     session.get("verified_res_id");
				 render(residence,landlord);
				 }
				 
				 else{System.out.print("Editing Page not Getting Value");
				 }
		        		    
		}
		  
		 
	 

	 public static void editsave(Residence residence){
				
		 residence.save();		 
		 editproperty();
	 }
	 
	 
	 public static void deleteproperty(){
			
		    String LLref =  session.get("logged_in_landlord");
		    Logger.info("Session user "+LLref);
		 
		    Landlord landlord = Landlords.getCurrentLandlord();
		    
		    List<Residence> residences = new ArrayList<Residence>();
		    // Fetch all residences and filter out those for landlord
		    List<Residence> residencesAll = Residence.findAll();
		    for (Residence residence : residencesAll)
		    {
		      
		      if (residence.landlord == landlord )
		      {
		        residences.add(residence);
		      }
		    }
		    render("Residences/deleteproperty.html", landlord,residences);
		    
    }	
	   
	 
	 
	 
	 public static void deletingpage(Residence residence, Landlord landlord){
		
		    authenticate(residence.id);
		    
		    if(authenticate(residence.id)==true){
		    
		      System.out.print(residence.id+"  will be deleted");
		      session.get("logged_in_resid");
		   
		      render(landlord, residence);
		      delete(residence,landlord);
		    }
		    
		    else{
		    	deleteproperty();
		    }
		    
		
	 } 
	 
	   
	 public static void delete(Residence residence,Landlord landlord){		 		 		   
		
		 authenticate(residence.id);
		    
		    if(authenticate(residence.id)==true){
		    
		      System.out.print(residence.id + " is the one");
		      session.get("logged_in_resid");		
		      residence.delete();
              deleteproperty();}

		    else{
		    	System.out.print("no joy");
		    	deleteproperty();}
	        	        
	 }	 
	 
	 public static void deletingerror() {
		render();
		
	}
	 
	 
	
	
	 public static void editresidency(Tenant tenant){
			
		    String TTref =  session.get("logged_in_tenant");
		    Logger.info("Session user "+TTref);
		    
		    Tenant tenante = Tenants.getCurrentTenant();
		    
		    List<Residence> residency = new ArrayList<Residence>();
		    List<Residence> vacancy = new ArrayList<Residence>();
		    //////////////////////////////////////////////////
		    //
		    
		    // Fetch all residences and filter out those with tenant id
		    List<Residence> residencesAll = Residence.findAll();
		    System.out.print(residencesAll);
		    
		    for (Residence residence : residencesAll)
		    {
		      
		      if (residence.tenant == tenante)
		      {
		        residency.add(residence);
		      }
		     
		      if (residence.tenant == null)
		      {
		        vacancy.add(residence);	
		        
		      }
		      
		      
		      
		    }
		    
		    
		    System.out.println(residency+" a");
		    System.out.println(vacancy+ " b");
		   // String jsonvacancy = new Gson().toJson(vacancy);
	       // System.out.println(jsonvacancy+ "c");
		    
		   
		    render("Residences/editresidency.html", tenant,residency,vacancy);
		    	    
	 }	
	 
	
	 public static void editingresidency(Residence residence){
						
		  authenticate(residence.id);
		  Logger.info("verified_res_id", +residence.id);
		  Tenant tenant = residence.tenant;		
		  Tenants.getCurrentTenant();
		  Landlord landlord = residence.landlord;
				 
				 if(authenticate(residence.id) == true){
			     session.get("verified_res_id");
			     session.get("logged_in_tenant");			     
				 render(residence,tenant,landlord);
				 revacancy(residence);
				 }
				 
				 else{System.out.print("Editing Page not Getting Value");}
		        
		      
		    
		}
	
	 
	 
	 public static void revacancy(Residence residence){
			
			
		  authenticate(residence.id);
		  Logger.info("verified_res_id : ", +residence.id);
		 	
		  Tenant tenant= Tenants.getCurrentTenant();
		  Landlord landlord = residence.landlord;
			System.out.print(residence.landlord);	 
                  if(authenticate(residence.id) == true)
                    
                  {
			       session.get("verified_res_id");
			       session.get("logged_in_tenant");			     
				   render(residence,tenant,landlord);
				  }
				 
				  else
				  {
				   Tenants.home();
				   System.out.print("Editing Page not Getting Value");
				  }
	        
	    }	        
		      
		    
	
	 public static void editsaveresidency(Residence residence,Tenant tenant,Landlord landlord){
		 
		 
		 
		 
		 System.out.print(tenant+" : "+residence+" : "+landlord);
		 System.out.print("Landlord"+landlord+"Tenant"+tenant+"Eircode"+ residence.eircode+ "Location "+residence.location+" Price " + residence.price +" Rooms "+residence.rooms+" Bathrooms "+residence.bathrooms+" Area "+residence.area+" Property Type " +residence.restype);
		 residence.delete();
		
		    if(residence.tenant == null&&residence.landlord!=null)			
		    {	        
				Residence res = new Residence(landlord,tenant,residence.eircode,residence.location,residence.price,residence.rooms,residence.bathrooms,residence.area,residence.restype);				
				res.save();
				Tenants.home();		       
		    }
		
		    if(residence.tenant != null&&residence.landlord!=null)
		    {	
		    	 
				 Tenant tenante = null;
			     Residence res = new Residence(landlord,tenante,residence.eircode,residence.location,residence.price,residence.rooms,residence.bathrooms,residence.area,residence.restype);				
			     res.save();
			     Tenants.home();
		   }
		
		
	   
		   else{
	    	deletingerror();
        
	        System.out.print("Houston we have a problem");
			
			System.out.print(tenant+" : "+residence+" : "+landlord);
		   
			Tenants.getCurrentTenant();
		    Tenants.home();
		   }
		    
	 
	 }
	 
	 public static List<Residence> vacantresidences(Tenant tenant){
			
		    String TTref =  session.get("logged_in_tenant");
		    Logger.info("Session user "+TTref);
		    
		    List<Residence> vacancy = new ArrayList<Residence>();
		    // Fetch all residences and filter out those with tenant id
		    List<Residence> residencesAll = Residence.findAll();
		    System.out.print(residencesAll);
		    
		    for (Residence residence : residencesAll)
		    {
		     
		      if (residence.tenant == null)
		      {
		        vacancy.add(residence);
		      }
		      
		    }
			return vacancy;
		  
		    
		    //render("Residences/editresidency.html", tenant,residency,vacancy);
		    	    
	 }	
	 

	public static boolean authenticate(long id){
			
		Logger.info("Attempt to Authenticate with "+id);
		
		Residence residence = Residence.findByID(id);
		 if (residence != null){
			 Logger.info("Successful Authentication of "+residence.id);
		 	 session.put("logged_in_resid",residence.id);
			 return true;
		 }
		
		 else{
			 Logger.info("Authentication Failed");
			 return false;
			 
		 }
	}

	 
	
}