package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
public class Landlords extends Controller {

public static void signup(){
        
	    session.clear();	
		render();
		
	}
public static void newlandlord(){
    
    session.clear();	
	render();
	
}

public static void login(){
	
	   session.clear();
	   render();
    }

public static void home(){
    
	render();
	
}


public static void logout(){
	
	   session.remove("logged_in_landlord");
	   Welcome.index();
}

public static void editprofile(){
	
	String LLref =  session.get("logged_in_landlord");
    Logger.info("Session user "+LLref);
    
    
    session.get("logged_in_landlord");
    Landlord landlord = Landlords.getCurrentLandlord();
	
    render(landlord);
	
}



	
	public static void register(Landlord landlord){
		
		
		
		if(landlord.conditions == null)
		{
	     Welcome.index();
		}
		if(landlord.conditions == true)
		{
		landlord.save();
		login();
		}
		if(landlord.conditions == false)
		{
	     Welcome.index();
		}
		if(landlord.conditions == true)
		{
		landlord.save();
		login();
		}
		
	}
	
public static void adminregister(Landlord landlord){
		
	landlord.conditions = true;
		
		if(landlord.conditions == true)
		{
	     landlord.save();
	     Administrators.home();
		}
}
	
	
	
	public static void reregister(Landlord landlord){
		
		
		
		landlord.save();
		editprofile();
	
	
	}
		

	
	public static void authenticate(String email, String password){
		
    Logger.info("Attempt to Authenticate with"+email+":"+password);
	
	Landlord landlord = Landlord.findByEmail(email);
	 if ((landlord != null)&&(landlord.checkPassword(password)==true)){
		 Logger.info("Successful Authentication of "+landlord.firstName);
	 	 session.put("logged_in_landlord",landlord.id);
		  Landlords.home();
	 }
	
	 else{
		 Logger.info("Authentication Failed");
		 login();
	 }
	}
	
	public static Landlord getCurrentLandlord(){
		
		String userId = session.get("logged_in_landlord");
		if (userId == null)
		{
			Logger.info("Failed to get Current User");
		return null;
		}
		Landlord loggered_in_landlord = Landlord.findById(Long.parseLong(userId));
		return loggered_in_landlord;
	}

public static Landlord getCurrentLandlordID(Landlord landlord){
		
		String resId = session.get("logged_in_landlord");
		if (resId == null)
		{
		Logger.info("Failed to get landlord");
		return null;
		}
		Landlord landlord_in_tables = Landlord.findById(Long.parseLong(resId));
		return landlord_in_tables;
	}	


}