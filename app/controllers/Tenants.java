package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import javax.persistence.PersistenceException;

import models.*;
public class Tenants extends Controller {

public static void signup(){
        
	    session.clear();	
		render();
		
	}

public static void newtenant(){
    
    session.clear();	
	render();
	
}

public static void login(){
	
	   session.clear();
	   render();
    }



public static void home(){
   
	
	String TTref =  session.get("logged_in_tenant");
    Logger.info("Session user "+TTref);
    
   
    
    
    session.get("logged_in_tenant");
    Tenant tenant = Tenants.getCurrentTenant();  
   
    
    render(tenant);
	
}



  public static void logout(){
	
	   session.remove("logged_in_tenant");
	   Welcome.index();
}

  
  public static void editprofile(Tenant tenant, List<Residence> residences){
	
	String TTref =  session.get("logged_in_tenant");
    Logger.info("Session user "+TTref);
    
    
    session.get("logged_in_tenant");
    Tenant tenante = Tenants.getCurrentTenant();    
    
    render(tenante);
	
}
	
	public static void register(Tenant tenant){
		
		
		
		if(tenant.conditions == null)
		{
	     Welcome.index();
		}
		if(tenant.conditions == true)
		{
		tenant.save();
		login();
		}
		if(tenant.conditions == false)
		{
	     Welcome.index();
		}
		
		
	}
	
	
	public static void adminregister(Tenant tenant){
		
		tenant.conditions = true;
			
			if(tenant.conditions == true)
			{
			 tenant.save();	
		     Administrators.home();
			}
	}
	
	public static void reregister(Tenant tenant){
		
		tenant.save();
		
	
	
	}
		

	
	public static void authenticate(String email, String password){
		
		Logger.info("Attempt to Authenticate with"+email+":"+password);
	
	   Tenant tenant = Tenant.findByEmail(email);
	   if ((tenant != null)&&(tenant.checkPassword(password)==true)){
		   		   
		    Logger.info("Successful Authentication of "+tenant.firstName);
	 	    session.put("logged_in_tenant",tenant.id);
	 	   
    		Tenants.home();
	   }
	
	 else{
		 Logger.info("Authentication Failed");
		 login();
	 }
}
	
	
	public static Tenant getCurrentTenant(){
		
		String userId = session.get("logged_in_tenant");
		if (userId == null)
		{
		Logger.info("Failed to get Current Tenant");
		return null;
		}
		
		Tenant loggered_in_tenant = Tenant.findById(Long.parseLong(userId));
		return loggered_in_tenant;
		
			
      }
  
	public static Tenant getCurrentTenantID(Tenant tenant){
		
		String resId = session.get("logged_in_tenant");
		if (resId == null)
		{
			Logger.info("Failed to get tenant");
		return null;
		}
		Tenant tenant_in_tables = Tenant.findById(Long.parseLong(resId));
		return tenant_in_tables;
	}	

	
	
	
	
}
	

