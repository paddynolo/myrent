package controllers;

import java.util.List;


import models.Residence;
import models.Tenant;
import models.Landlord;
import play.Logger;
import play.mvc.Controller;

public class InputData extends Controller{

	
	public static void index(){
		
		Landlord landlord = Landlords.getCurrentLandlord();
		if(landlord == null){
			Logger.info("inputData class : unable to getCurrentuser");
			Landlords.login();
		}
		else{
			
			render(landlord);
		}
	}
	
public static void data(Landlord landlord,Tenant tenant,String eircode,String location, float price, int rooms,int bathrooms,int area, String restype){
		
	    String LLref =  session.get("logged_in_landlord");
	    Logger.info("Session user "+LLref);
    	addData(landlord,tenant,eircode,location,price,rooms,bathrooms,area,restype);
    	Logger.info("Landlord"+landlord+"Tenant"+tenant+"Eircode"+ eircode+ "Location "+location+" Price " + price +" Rooms "+rooms+" Bathrooms "+bathrooms+" Area "+area+" Property Type " +restype);


    index();
   }
        

public static void addData(Landlord landlord,Tenant tenant,String eircode,String location, float price, int rooms,int bathrooms,int area, String restype){
	
    Landlord landlorde = Landlords.getCurrentLandlord();
    Tenant tenante = null;
	System.out.print(landlorde+":"+tenante);
	if(landlorde != null){
	
	Residence res = new Residence(landlorde,tenante,eircode,location,price,rooms,bathrooms,area,restype);
	res.save();}
	
	session.get("logged_in_landlord");
	InputData.index();
	
}



 public static void renderReport(){
	
	List<Residence>residences = Residence.findAll();
	render(residences);
	
  }

}
