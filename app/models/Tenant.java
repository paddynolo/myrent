package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import controllers.Landlords;
import controllers.Residences;
import controllers.Tenants;
import controllers.InputData;
import play.db.jpa.Model;
import utils.LatLng;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;

import java.util.*;

import models.*;


@Entity
public class Tenant extends Model{

	
	
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public Boolean conditions;
	
	@OneToOne(mappedBy = "tenant", cascade=CascadeType.ALL)    	
	@PrimaryKeyJoinColumn	
	public Residence residence;
	
	
	public Tenant (Boolean conditions, String firstName, String lastName,
			String email, String password){
			
		
		
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.conditions = conditions;
		
		
	}
	
    public static Tenant findByEmail(String email){
		
		return find("email", email).first();
		
	}

	public boolean checkPassword(String password){
		
		return this.password.equals(password);
	}
	
    /*public boolean checkResidence(Residence residence){
		
		return this.residence.equals(residence);
	}*/


    
    
}
