package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import controllers.Landlords;
import controllers.ContactController;
import models.Landlord;
import play.db.jpa.Model;
import play.mvc.Controller;


@Entity
public class Contact extends Model{
	
		
	public String email;
	public String message;
	
	@ManyToOne
	@JoinColumn(name="sender_id")
	public Landlord sender;
	
	public Contact(Landlord sender,String email, String message)
	
	{
		
		this.sender=Landlords.getCurrentLandlord();
		this.email=email;
		this.message=message;
	}
	
	public Contact(Tenant sendert,String email, String message)
	
	{
		
		this.sender=Landlords.getCurrentLandlord();
		this.email=email;
		this.message=message;
	}
	

}

