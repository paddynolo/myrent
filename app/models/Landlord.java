package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;

import javax.persistence.Entity;

import javax.persistence.OneToMany;

import controllers.Landlords;
import play.db.jpa.Model;

@Entity
public class Landlord extends Model {

	
	
	public String firstName;
	public String lastName;
	public String address_1;
	public String address_2;
	public String email;
	public String password;
	public Boolean conditions;
    
	@OneToMany(mappedBy ="landlord", cascade = CascadeType.ALL)
	
	List<Residence>residences = new ArrayList <Residence>();
	
	@OneToMany(mappedBy = "sender",cascade = CascadeType.ALL)
	
	List<Contact>contacts = new ArrayList <Contact>();

	
    

	public Landlord (Boolean conditions, String firstName, String lastName,
			String address_1, String address_2,String email, String password){
		
	
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.address_1 = address_1;
		this.address_2 = address_2;
		this.email = email;
		this.password = password;
		this.conditions = conditions;
		
	} 


	public static Landlord findByEmail(String email){
		
		return find("email", email).first();
		
	}

	public boolean checkPassword(String password){
		
		return this.password.equals(password);
	}


		
	
}
