package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;

import javax.persistence.Entity;

import javax.persistence.OneToMany;

import controllers.Landlords;
import play.db.jpa.Model;

@Entity
public class Administrator extends Model {

	
	
	public String admin;
	public String email;
	public String password;
	
    

	public Administrator(String admin,String email, String password){
		
	
		
		this.admin = admin;
		this.email = "admin@witpress.ie";
		this.password = "secret";
		
	} 


	public static Administrator findByEmail(String email){
		
		return find("email", email).first();
		
	}

	public boolean checkPassword(String password){
		
		return this.password.equals(password);
	}


		
	
}
