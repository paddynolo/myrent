package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;



import controllers.Landlords;
import controllers.Tenants;
import controllers.InputData;
import play.db.jpa.Model;
import utils.LatLng;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;

import java.util.*;

import models.*;


@Entity
public class Residence extends Model{

	
	public String  eircode;
	public String  location;
	public float   price;
	public int     rooms;
	public int     bathrooms;
	public int     area;
	public String  restype;
	
    
	@ManyToOne
	@JoinColumn(name="landlord_id")
	public Landlord landlord;
	
	
		
	@OneToOne
	@JoinColumn(name="tenant_id")
	public Tenant tenant;
	
	
	public Residence(Landlord landlord,Tenant tenant,String eircode,String location, float price, int rooms,int bathrooms,int area, String restype){
		
		
		
		this.landlord=landlord;//Landlords.getCurrentLandlord();
		this.tenant=tenant;//Tenants.getCurrentTenant();
		this.eircode=eircode;
		this.location=location;
		this.price=price;
		this.rooms=rooms;
		this.bathrooms=bathrooms;
		this.area=area;
		this.restype=restype;
		
	}
	
	public static Residence findByEircode(String eircode){
		//what is .first() doing
		return find("eircode", eircode).first();
		
	}
	
    public static Residence findByLocation(String location){
		//what is .first() doing
		return find("location", location).first();
		
	}
    
    public static Residence findByID(long id){
		//what is .first() doing
		return find("id", id).first();
		
	}
	
    
    public LatLng getGeolocation(){
    	
	  return LatLng.toLatLng(location);
	  
    }
    
    
}
